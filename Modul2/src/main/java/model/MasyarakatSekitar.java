
package model;


public class MasyarakatSekitar extends Penduduk{
    private String nomor;
    
    public MasyarakatSekitar(){
        
    }
    public MasyarakatSekitar(String nomor, String nama, String tempatTanggalLahir){
        super(nama, tempatTanggalLahir);
        this.nomor=nomor;
    }
    public void setNomor(String nomor){
        this.nomor=nomor;
    }
    public String getNomor(){
        return nomor;
    }

    @Override
    public double hitungIuaran() {
        return Integer.parseInt(nomor)*100;
    }
    
    
}
