
package model;
public abstract class Penduduk {
    private String nama;
    private String tempatTanggalLahir;
    
    public Penduduk(){
        
    }
    public Penduduk(String nama, String tempatTanggalLahir){
        this.nama=nama;
        this.tempatTanggalLahir=tempatTanggalLahir;
    }
    public void setNama(String nama){
        this.nama=nama;
    }
    public String getNama(){
        return nama;
    }
    public void setTempatTanggalLahir(String tempatTanggalLahir){
        this.tempatTanggalLahir=tempatTanggalLahir;
    }
    public String getTampatTanggalLahir(){
        return tempatTanggalLahir;
    }
    public abstract double hitungIuaran();
}
