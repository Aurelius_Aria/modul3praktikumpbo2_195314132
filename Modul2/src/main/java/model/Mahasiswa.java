
package model;


public class Mahasiswa extends Penduduk{
    private String nim;
    
    public Mahasiswa(){
        
    }
    public Mahasiswa(String nim,String nama, String tempatTanggalLahir){
        super(nama, tempatTanggalLahir);
    }
    public void setNim(String nim){
        this.nim=nim;
    }
    public String getNim(){
        return nim;
    }

    @Override
    public double hitungIuaran() {
        return Integer.parseInt(nim)/10000;
    }
    
    
}
