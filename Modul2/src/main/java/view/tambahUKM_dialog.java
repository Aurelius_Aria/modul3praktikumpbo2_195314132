
package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class tambahUKM_dialog extends JDialog implements ActionListener{
    private JLabel label_judul;
    private JButton tombol;
    private JLabel label_namaUnit;
    private JLabel label_ketua;
    private JLabel label_sekretaris;
    private JLabel label_penduduk;
    private JTextField text_namaUnit;
    private JTextField text_ketua;
    private JTextField text_sekretaris;
    private JComboBox comBox_penduduk;
    private String pendudukArray[] = {"Aria","Chriss", "Dimas", "Felix"};
    
    public tambahUKM_dialog(){
        tambah();
    }
    
    public void tambah(){
        this.setLayout(null);
        
        label_judul = new JLabel("Form Tambah UKM");
        label_judul.setBounds(110, 10, 200, 20);
        this.add(label_judul);
        
        label_namaUnit = new JLabel("Nama unit\t :");
        label_namaUnit.setBounds(80, 60, 100, 50);
        this.add(label_namaUnit);
        text_namaUnit = new JTextField();
        text_namaUnit.setBounds(180, 75, 180, 20);
        this.add(text_namaUnit);
        
        label_ketua = new JLabel("Ketua \t :");
        label_ketua.setBounds(80, 100, 100, 50);
        this.add(label_ketua);
        text_ketua = new JTextField();
        text_ketua.setBounds(180, 115, 180, 20);
        this.add(text_ketua);
        
        label_sekretaris = new JLabel("Sekretaris\t :");
        label_sekretaris.setBounds(80, 140, 100, 50);
        this.add(label_sekretaris);
        text_sekretaris = new JTextField();
        text_sekretaris.setBounds(180, 155, 180, 20);
        this.add(text_sekretaris);
        
        label_penduduk = new JLabel("Penduduk \t :");
        label_penduduk.setBounds(80, 180, 100, 50);
        this.add(label_penduduk);
        comBox_penduduk = new JComboBox(pendudukArray);
        comBox_penduduk.setBounds(180, 195, 180, 20);
        this.add(comBox_penduduk);
        
        tombol = new JButton("OK");
        tombol.setBounds(150, 250, 100, 30);
        this.add(tombol);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
