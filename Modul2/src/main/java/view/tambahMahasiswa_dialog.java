
package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import model.Mahasiswa;


public class tambahMahasiswa_dialog extends  JDialog implements ActionListener{
    private JLabel label_judul;
    private JButton tombol;
    private JLabel label_name;
    private JLabel label_nim;
    private JLabel label_ttl;
    private JTextField text_name;
    private JTextField text_nim;
    private JTextField text_ttl;
    
    public tambahMahasiswa_dialog(){
        tambah();
    }
    
    public void tambah(){
        this.setLayout(null);
        
        label_judul= new JLabel("Form Tambah Mahasiswa");
        label_judul.setBounds(120, 10, 200, 20);
        this.add(label_judul);
        
        label_name=new JLabel("Nama \t :");
        label_name.setBounds(122, 60, 100, 50);
        this.add(label_name);
        text_name=new JTextField();
        text_name.setBounds(180, 75, 180, 20);
        this.add(text_name);
        
        label_nim=new JLabel("NIM \t :");
        label_nim.setBounds(133, 100, 100, 50);
        this.add(label_nim);
        text_nim=new JTextField();
        text_nim.setBounds(180, 115, 180, 20);
        this.add(text_nim);
        
        label_ttl=new JLabel("Tempat & Tanggal Lahir \t :");
        label_ttl.setBounds(20, 155, 200, 20);
        this.add(label_ttl);
        text_ttl=new JTextField();
        text_ttl.setBounds(180, 155, 180, 20);
        this.add(text_ttl);
        
        tombol=new JButton("OK");
        tombol.setBounds(150, 200, 100, 30);
        this.add(tombol);
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == tombol){
            Mahasiswa mhs= new Mahasiswa();
            mhs.setNama(text_name.getText());
            mhs.setNim(text_nim.getText());
            mhs.setTempatTanggalLahir(text_ttl.getText());
        }
    }
    
}
