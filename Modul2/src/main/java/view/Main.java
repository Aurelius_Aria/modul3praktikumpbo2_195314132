
package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author acer
 */
public class Main extends JFrame implements ActionListener{
    private JMenuBar menuBar;
    private JMenuItem menuItem_tambahMhs;
    private JMenuItem menuItem_tambahMasyarakat;
    private JMenuItem menuItem_tambahUKM;
    private JMenuItem menufile_lihatData;
    private JMenuItem menu_exit;
    private JMenu menu_File;
    private JMenu menu_Edit;
    private JMenu menu_Help;
    
    public Main(){
        tambahComponents();
    }
    
    private void tambahComponents(){
        menuBar= new JMenuBar();
        menu_File= new JMenu("File");
        menu_Edit= new JMenu("Edit");
        menu_Help= new JMenu("Help");
        
        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);
        this.setJMenuBar(menuBar);
        
        menuItem_tambahMhs=new JMenuItem("Tambah Mahasiswa");
        menuItem_tambahMasyarakat = new JMenuItem("Tambah Masyarakat");
        menuItem_tambahUKM = new JMenuItem("Tambah UKM");
        menufile_lihatData = new JMenuItem("Lihat Data");
        menu_exit = new JMenuItem("Exit");
        
        menu_Edit.add(menuItem_tambahMhs);
        menu_Edit.add(menuItem_tambahMasyarakat);
        menu_Edit.add(menuItem_tambahUKM);
        
        menu_File.add(menufile_lihatData);
        menu_File.add(menu_exit);
        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);
        
        menu_exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        
        menuItem_tambahMhs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahMahasiswa_dialog addMhs= new tambahMahasiswa_dialog();
                addMhs.setSize(400, 300);
                addMhs.setVisible(true);
            }
        });
        
        menuItem_tambahMasyarakat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahMasyarakat_dialog addMasyarakat = new tambahMasyarakat_dialog();
                addMasyarakat.setSize(400, 300);
                addMasyarakat.setVisible(true);
            }
        });
        
        menuItem_tambahUKM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahUKM_dialog addUKM= new tambahUKM_dialog();
                addUKM.setSize(400, 469);
                addUKM.setVisible(true);
            }
        });
    }
    
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Main main=new Main();
                main.setSize(400, 500);
                main.setVisible(true);
                main.setResizable(false);
                main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
    }
    
}
