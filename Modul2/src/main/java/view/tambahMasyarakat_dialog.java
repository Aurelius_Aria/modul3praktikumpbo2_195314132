
package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import model.MasyarakatSekitar;


public class tambahMasyarakat_dialog extends JDialog implements  ActionListener{
    private JLabel label_judul;
    private JButton tombol;
    private JLabel label_name;
    private JLabel label_nomor;
    private JLabel label_ttl;
    private JTextField text_name;
    private JTextField text_nomor;
    private JTextField text_ttl;
    
    public tambahMasyarakat_dialog(){
        tambah();
    }
    
    public void tambah(){
        this.setLayout(null);
        
        label_judul= new JLabel("Form Tambah Masyarakat");
        label_judul.setBounds(110, 10, 200, 20);
        this.add(label_judul);
        
        label_name=new JLabel("Nama \t :");
        label_name.setBounds(122, 60, 100, 50);
        this.add(label_name);
        text_name=new JTextField();
        text_name.setBounds(180, 75, 180, 20);
        this.add(text_name);
        
        label_nomor=new JLabel("Nomor \t :");
        label_nomor.setBounds(117, 100, 100, 50);
        this.add(label_nomor);
        text_nomor=new JTextField();
        text_nomor.setBounds(180, 115, 180, 15);
        this.add(text_nomor);
        
        label_ttl=new JLabel("Tempat & Tanggal Lahir \t :");
        label_ttl.setBounds(20, 155, 200, 20);
        this.add(label_ttl);
        text_ttl=new JTextField();
        text_ttl.setBounds(180, 155, 180, 20);
        this.add(text_ttl);
        
        tombol=new JButton("OK");
        tombol.setBounds(150, 200, 100, 30);
        this.add(tombol);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == tombol){
            MasyarakatSekitar mas= new MasyarakatSekitar();
            mas.setNama(text_name.getText());
            mas.setNomor(text_nomor.getText());
            mas.setTempatTanggalLahir(text_ttl.getText());
        }
    }
    
}
