
package com.mycompany.praktikummodul1;

public class Main {
    public static void main(String[] args) {
        
        UKM objUKM = new UKM();
        Penduduk[] anggota= new Penduduk[5];
        int a=0;
        
        System.out.println("UKM Sanata Dharma");
        System.out.println("-----------------");
        
        Mahasiswa ketua =new Mahasiswa();
        ketua.setNama("Tony Stark");
        ketua.setNim("195314132");
        ketua.setTempatTanggalLahir("Jogja, 12 juni 2001");
        
        Mahasiswa sekre =new Mahasiswa();
        sekre.setNama("Chriss");
        sekre.setNim("195314129");
        sekre.setTempatTanggalLahir("Purwekerto, 10 oktober 2001");
        
        objUKM.setKetua(ketua);
        objUKM.setSekretaris(sekre);
        objUKM.setNamaUnit("Fotografi");
        
        System.out.println(""+objUKM.getNamaUnit());
        System.out.println("-------------------------------------");
        System.out.println("Ketua");
        System.out.println("Ketua                   : "+objUKM.getKetua().getNama());
        System.out.println("Nim                     : "+objUKM.getKetua().getNim());
        System.out.println("Tempat Tanggal lahir    : "+objUKM.getKetua().getTempatTanggaLahir());
        System.out.println("");
        
        System.out.println("Sekretaris");
        System.out.println("Sekretaris              : "+objUKM.getSekretaris().getNama());
        System.out.println("Nim                     : "+objUKM.getSekretaris().getNim());
        System.out.println("Tempat Tanggal Lahir    : "+objUKM.getSekretaris().getTempatTanggaLahir());
        System.out.println("-------------------------------------");
        System.out.println("");
        
        anggota[a]= new Mahasiswa();
        ((Mahasiswa)anggota[a]).setNama("Thor");
        ((Mahasiswa)anggota[a]).setNim("195314124");
        ((Mahasiswa)anggota[a]).setTempatTanggalLahir("Jogja, 12 april 1999");
        a++;
        
        anggota[a]= new MasyarakatSekitar();
        ((MasyarakatSekitar)anggota[a]).setNama("Hulk");
        ((MasyarakatSekitar)anggota[a]).setNomor("165");
        ((MasyarakatSekitar)anggota[a]).setTempatTanggalLahir("Jakarta, 12 Desember 2000");
        a++;
        
        for (int i = 0; i < a; i++) {
            if (anggota[i]instanceof Mahasiswa){
                System.out.println("Mahasiswa");
                System.out.println("Nama            : "+anggota[i].getNama());
                System.out.println("Nim             : "+((Mahasiswa)anggota[i]).getNim());
                System.out.println("Tanggal Lahir   : "+anggota[i].getTempatTanggaLahir());
                System.out.println("Iuran           : RP "+((Mahasiswa)anggota[i]).hitungIuran());
                System.out.println("");
            }
            else {
                System.out.println("Masyarakat");
                System.out.println("Nama            : "+anggota[i].getNama());
                System.out.println("Nomor KT        : "+((MasyarakatSekitar)anggota[i]).getNomor());
                System.out.println("Tanggal Lahir   : "+anggota[i].getTempatTanggaLahir());
                System.out.println("Iuran           : RP "+((MasyarakatSekitar)anggota[i]).hitungIuran());
                System.out.println("");
            }
        }
    }
}
