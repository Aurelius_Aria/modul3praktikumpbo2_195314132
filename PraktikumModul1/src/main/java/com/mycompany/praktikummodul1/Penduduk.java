
package com.mycompany.praktikummodul1;

public abstract class Penduduk {
    protected String nama;
    protected String tempatTanggalLahir;
    
    public Penduduk(){
        
    }
    public Penduduk(String nama, String tempatTanggalLahir){
        this.nama=nama;
        this.tempatTanggalLahir=tempatTanggalLahir;
    }
    public void setNama(String nama){
        this.nama=nama;
    }
    public String getNama(){
        return nama;
    }
    public void setTempatTanggalLahir(String tempatTangglLahir){
        this.tempatTanggalLahir=tempatTangglLahir;
    }
    public String getTempatTanggaLahir(){
        return tempatTanggalLahir;
    }
    public abstract double hitungIuran();
}
