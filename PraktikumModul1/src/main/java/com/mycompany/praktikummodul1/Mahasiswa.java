
package com.mycompany.praktikummodul1;

public class Mahasiswa extends Penduduk{
    private String nim;
    
    public Mahasiswa(){
        
    }
    public Mahasiswa(String nim, String nama, String tempatTanggalLahir){
        super(nama, tempatTanggalLahir);
        this.nim=nim;
    }
    public void setNim(String nim){
        this.nim=nim;
    }
    public String getNim(){
        return nim;
    }
    @Override
    public double hitungIuran() {
        return Integer.parseInt(getNim())/10000;
    }
    
}
