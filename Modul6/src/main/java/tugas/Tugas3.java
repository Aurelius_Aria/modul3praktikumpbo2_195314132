
package tugas;

import java.util.Scanner;

public class Tugas3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        String input, reverse="";
        
        System.out.println("Masukan kata yang akan di cek palondrom: ");
        input =sc.nextLine();
        
        int length=input.length();
        
        for (int i = length - 1; i >= 0; i--) {
            reverse= reverse+input.charAt(i);
        }
        System.out.println("Input Kata : "+input);
        System.out.println("Reverse kata: "+reverse);
        if (input.equals(reverse)) {
            System.out.println("Status : Palindrom");
        }
        else{
            System.out.println("Status : Bukan Palindrom");
        }
    }
}
