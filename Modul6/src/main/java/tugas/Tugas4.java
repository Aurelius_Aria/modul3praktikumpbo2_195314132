
package tugas;

import java.util.Scanner;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;



public class Tugas4 {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        
        String sentence = JOptionPane.showInputDialog("Input Kalimat:");
        
        StringTokenizer strTokens;
        strTokens = new StringTokenizer(sentence);
        
        System.out.println("Inputan String : "+sentence);
        System.out.println("Jumlah kata : "+strTokens.countTokens());
    }
}
