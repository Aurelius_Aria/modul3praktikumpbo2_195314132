
package tugas;


public class Tugas2 {
    public static void main(String[] args) {
        String s1 = new String("Aurelius ");
        String s2 = new String("Aria ");
        String s3 = new String("Baras ");
        
        System.out.printf("First Name: %s\nMiddle Name: %s\nLast Name: %s\n\n ", s1,s2,s3);
        System.out.printf("Full Name: %s\n ", s1.concat(s2.concat(s3)));
        
    }
}
