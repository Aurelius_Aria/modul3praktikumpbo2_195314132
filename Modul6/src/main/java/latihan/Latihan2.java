
package latihan;

import javax.swing.JOptionPane;


public class Latihan2 {
    private static final char BLANK = ' ';
    
    public static void main(String[] args) {
        int index, wordCount, numberOfCharacters;
        
        String sentence = JOptionPane.showInputDialog("Input Kalimat:");
        
        numberOfCharacters = sentence.length();
        index              = 0;
        wordCount          = 0;
        
        while (index < numberOfCharacters) {            
            while (index < numberOfCharacters && sentence.charAt(index) == BLANK) {                
                index++;
            }
            while (index < numberOfCharacters && sentence.charAt(index) !=BLANK) {                
                index++;
            }
            wordCount++;
        }
        System.out.println("\nInput sentence: "+sentence);
        System.out.println("   Word count: "+ wordCount + " words");
    }
}
