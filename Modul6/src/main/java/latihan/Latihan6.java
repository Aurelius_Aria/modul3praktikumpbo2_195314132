
package latihan;

import java.util.Scanner;
import java.util.StringTokenizer;


public class Latihan6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Masukan sebuah kalimat");
        String sentence = scanner.nextLine();
        
        StringTokenizer tokens = new StringTokenizer(sentence);
        
        System.out.println("Kalimat anda terdiri dari kata-kata berikut ini: ");
        while (tokens.hasMoreTokens()) {            
            System.out.println(tokens.nextToken());
        }
    }
}
