/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihan;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;


public class Latihan4 extends JFrame implements ActionListener{
    private static final int FRAME_WIDTH= 300;
    private static final int FRAME_HEIGHT= 200;
    private static final int FRAME_X_ORIGIN= 150;
    private static final int FRAME_y_ORIGIN= 250;
    private static final int BUTTON_WIDTH= 80;
    private static final int BUTTON_HIEGHT= 30;
    private JButton button;
    private JButton button2;
    private JTextField pesan;
    
    public static void main(String[] args) {
        Latihan4 frame= new Latihan4();
        frame.setVisible(true);
    }
    public Latihan4(){
        Container contentPane= getContentPane();
        contentPane.setLayout(new FlowLayout());
        
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setTitle("Program Ch14JButtonFrame");
        setLocation(FRAME_X_ORIGIN, FRAME_y_ORIGIN);
        
        button= new JButton("Click Me");
        button.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        contentPane.add(button);
        button2 =new JButton("Tombol2");
        button2.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        contentPane.add(button2);
        pesan = new JTextField();
        pesan.setColumns(20);
        contentPane.add(pesan);
        
        button.addActionListener(this);
        button2.addActionListener(this);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton buttonText= (JButton) e.getSource();
        
        JRootPane rootPane= buttonText.getRootPane();
        Frame frame =(JFrame) rootPane.getParent();
       
        if (buttonText.getText().equals("Click Me")){
            String isiPesan= pesan.getText();
            frame.setTitle(isiPesan);
        }
        else {
            frame.setTitle("You cliked " + buttonText.getText());
        }
    }
}
