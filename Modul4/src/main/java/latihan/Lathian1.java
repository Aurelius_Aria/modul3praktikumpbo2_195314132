package latihan;

import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

public class Lathian1 extends JFrame{
    private static final int FRAME_WIDTH= 300;
    private static final int FRAME_HEIGHT= 200;
    private static final int FRAME_X_ORIGIN= 150;
    private static final int FRAME_y_ORIGIN= 250;
    private static final int BUTTON_WIDTH= 80;
    private static final int BUTTON_HIEGHT= 30;
    private JButton button;
    
    public static void main(String[] args) {
        Lathian1 frame= new Lathian1();
        frame.setVisible(true);
    }
    public Lathian1(){
        Container contentPane= getContentPane();
        contentPane.setLayout(new FlowLayout());
        
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setTitle("Program Ch7JButtonFrame");
        setLocation(FRAME_X_ORIGIN, FRAME_y_ORIGIN);
        
        button= new JButton("Click Me");
        button.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        contentPane.add(button);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        ButtonHandler handler = new ButtonHandler();
        button.addActionListener(handler);
    }
}
