
package latihan;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRootPane;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;


public class Latihan2 extends JFrame implements ActionListener{
    private static final int FRAME_WIDTH= 300;
    private static final int FRAME_HEIGHT= 200;
    private static final int FRAME_X_ORIGIN= 150;
    private static final int FRAME_y_ORIGIN= 250;
    private static final int BUTTON_WIDTH= 80;
    private static final int BUTTON_HIEGHT= 30;
    private JButton button;
    
    public static void main(String[] args) {
        Latihan2 frame= new Latihan2();
        frame.setVisible(true);
    }
    public Latihan2(){
        Container contentPane= getContentPane();
        contentPane.setLayout(new FlowLayout());
        
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setTitle("Program Ch14JButtonFrame");
        setLocation(FRAME_X_ORIGIN, FRAME_y_ORIGIN);
        
        button= new JButton("Click Me");
        button.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        contentPane.add(button);
        
        button.addActionListener(this);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clikedButton= (JButton) e.getSource();
        
        JRootPane rootPane= clikedButton.getRootPane();
        Frame frame =(JFrame) rootPane.getParent();
        String buttonText = clikedButton.getText();
        
        frame.setTitle("(Dibuat dengan cara - 2)" + buttonText);
    }
    
}
