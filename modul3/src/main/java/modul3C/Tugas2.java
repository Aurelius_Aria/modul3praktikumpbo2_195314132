
package modul3C;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;


public class Tugas2 extends JDialog{
    private static final int FRAME_WIDTH=400;
    private static final int FRAME_HEIGHT=400;
    
    public Tugas2(){
        JButton yellow;
        JButton blue;
        JButton red;
        
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setTitle("ButtonTest");
        Container contentPane;
        setLayout(new FlowLayout());
        contentPane=getContentPane();
        
        yellow=new JButton("YELLOW");

        blue=new JButton("BLUE");

        red = new JButton("RED");

        
        contentPane.add(yellow);
        contentPane.add(blue);
        contentPane.add(red);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
    public static void main(String[] args) {
        Tugas2 dialog= new Tugas2();
        dialog.setVisible(true);
    }
            
}

