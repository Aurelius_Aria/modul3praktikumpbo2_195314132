
package modul3C;

import java.net.URL;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;


public class Tugas3 extends JDialog{
    private static final int FRAME_WIDTH=400;
    private static final int FRAME_HEIGHT=400;
    private JLabel labelGambar;
    private JLabel namaGambar;
    
    public static void main(String[] args) {
        Tugas3 dialog=new Tugas3();
        dialog.setVisible(true);
        dialog.setTitle("Text And Icon Label");
    }
    
    public Tugas3(){
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        inGambar();
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
    public void inGambar(){
        this.setLayout(null);
        
        URL grape=this.getClass().getResource("grape.jpg");
        ImageIcon image= new ImageIcon();
        labelGambar=new JLabel(image);
        labelGambar.setBounds(100, 110, 250, 200);
        this.add(labelGambar);
        
        namaGambar=new JLabel("Grapes");
        namaGambar.setBounds(200, 330, 80, 30);
        this.add(namaGambar);
    }
}
