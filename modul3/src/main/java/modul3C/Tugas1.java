
package modul3C;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;


public class Tugas1 extends JFrame{
    private JMenu file;
    private JMenu edit;
    private JMenuBar menu;
    private JMenuItem tampil1;
    private JMenuItem tampil2;
    
    public  Tugas1(){
        menu =new JMenuBar();
        file= new JMenu("File");
        edit= new JMenu("Edit");
        tampil1=new JMenuItem("Tampil 1");
        tampil2=new JMenuItem("Tampil 2");
        
        menu.add(file);
        menu.add(edit);
        file.add(tampil1);
        file.add(tampil2);
        menu.add(file);
        
        this.setJMenuBar(menu);
        
        setSize(300, 300);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        
        
    }
    public static void main(String[] args) {
        Tugas1 frame= new Tugas1();
        frame.setVisible(true);
        
        
    }
}
