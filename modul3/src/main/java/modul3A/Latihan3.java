
package modul3A;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


public class Latihan3 extends JFrame{
    
    public Latihan3(){
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);
        
        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("Ini Tombol");
        panel.add(tombol);
        this.add(panel);
        
        JLabel label = new JLabel(" Label");
        panel.add(label);
        this.add(panel);
        JTextField text= new JTextField();
        panel.add(text);
        this.add(panel);
        JCheckBox chekBox= new JCheckBox();
        panel.add(chekBox);
        this.add(panel);
        JRadioButton radio= new JRadioButton();
        panel.add(radio);
        this.add(panel);
        
        
        
        
        
    }
    
    public static void main(String[] args) {
        new Latihan3();
    }
}
