
package modul3A;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Latihan4 extends JFrame{
    
    public Latihan4(){
        this.setLayout(null);
        this.setSize(400, 200);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);
        
        
        JLabel label = new JLabel("Keyword:");
        label.setBounds(150, 20, 200, 20);
        this.add(label);
        
        JTextField text = new JTextField();
        text.setBounds(20,50, 350, 20);
        this.add(text);
        
        JButton button= new JButton("Find");
        button.setBounds(120, 80, 100, 20);
        this.add(button);
        
    }
    
    public static void main(String[] args) {
        new Latihan4();
    }
}
