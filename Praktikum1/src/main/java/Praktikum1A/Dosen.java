
package Praktikum1A;


public class Dosen extends Peminjam{
    private String nomorIndukPegawai;
    
    public Dosen(String nomorIndukPegawai, String nama, String alamat){
        super(nama, alamat);
        this.nomorIndukPegawai=nomorIndukPegawai;
    }
    public void setNomorIndukPegawai(String nomorIndukPegawai){
        this.nomorIndukPegawai=nomorIndukPegawai;
    }
    public String getNomorIndukPwgegawai(){
        return nomorIndukPegawai;
    }
}
