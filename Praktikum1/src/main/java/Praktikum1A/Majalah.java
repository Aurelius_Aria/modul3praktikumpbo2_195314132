
package Praktikum1A;

public class Majalah extends Perpustakaan{
    private String ISSN;
    private String volume;
    private String series;
    
    public Majalah(String ISSN,String volume,String series,String id_koleksi, String judul, String penerbit,String tahunTerbit){
        super(id_koleksi, judul, penerbit, tahunTerbit);
        this.ISSN= ISSN;
        this.volume=volume;
        this.series=series;
    }
    public void setISSN(String ISSN){
        this.ISSN= ISSN;
    }
    public String getISSN(){
        return ISSN;
    }public void setVolume(String volume){
        this.volume=volume;
    }
    public String getVolume(){
        return volume;
    }public void setSeries(String Iseries){
        this.series=series;
    }
    public String getISBN(){
        return series;
    }
}
