
package Praktikum1A;


public class MasyarakatUmum extends Peminjam{
    private String nomorKTP;
    
    public MasyarakatUmum(String nomorKTP, String nama, String Alamat){
        super(nama, Alamat);
        this.nomorKTP=nomorKTP;
    }
    public void setNomorKTP(String nomorKTP){
        this.nomorKTP=nomorKTP;
    }
    public String getNomorKTP(){
        return nomorKTP;
    }
}
