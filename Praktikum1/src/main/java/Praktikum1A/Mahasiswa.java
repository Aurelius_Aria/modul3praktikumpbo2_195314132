package Praktikum1A;


public class Mahasiswa extends Peminjam{
    private String nomorMahasiswa;
    
    public Mahasiswa(){
        
    }
    
    public Mahasiswa(String nomorMahasiswa, String nama,String alamat){
        super(nama, alamat);
        this.nomorMahasiswa=nomorMahasiswa;
    }
    public void setNomorMahasiswa(String nomorMAhasiswa){
        this.nomorMahasiswa=nomorMAhasiswa;
    }
    public String getNomorMahasiswa(){
        return nomorMahasiswa;
    }
}
