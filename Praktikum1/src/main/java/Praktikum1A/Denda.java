
package Praktikum1A;

public class Denda {
    private final int MAKS_HARI_PINJAM =7;
    private final int DENDA_PER_HARI=500;
    private int jumlahHariSewa;
    private int jmlhPinjam;
    private Mahasiswa objPeminjam1;
    private Dosen objPeminjam2;
    private MasyarakatUmum objPeminjam3;
    private Perpustakaan objPerpus;
    
    public Denda(){
        
    }
    
    public Denda(int jumlahHariSewa, int jmlhPinjam){
        this.jumlahHariSewa=jumlahHariSewa;
        this.jmlhPinjam=jmlhPinjam;
        
    }
    
    public void setJumlahHariSewa(int jumlahHariSewa){
        this.jumlahHariSewa=jumlahHariSewa;
    }
    public int getJumlahHariSewa(){
        return jumlahHariSewa;
    }
    public void setObjPeminjam1(Mahasiswa objPeminjam1){
        this.objPeminjam1=objPeminjam1;
    }
    public Mahasiswa getObjPeminjam1(){
        return objPeminjam1;
    }
    public void setObjPeminjam2(Dosen objPeminjam2){
        this.objPeminjam2=objPeminjam2;
    }
    public Dosen getObjPeminjam2(){
        return objPeminjam2;
    }
    public void setObjPeminjam3(MasyarakatUmum objPeminjam3){
        this.objPeminjam3=objPeminjam3;
    }
    public MasyarakatUmum getObjPeminjam3(){
        return objPeminjam3;
    }
    public void setJmlhhPinjam(int jmlhPinjam){
        this.jmlhPinjam=jmlhPinjam;
    }
    public int getjmlhPinjaam(){
        return jmlhPinjam;
    }
    public void setObjPerpus(Perpustakaan objPerpus){
        this.objPerpus=objPerpus;
    }
    public Perpustakaan getObjPerpus(){
        return objPerpus;
    }
    public int hitugDenda(){
       if(jumlahHariSewa<= MAKS_HARI_PINJAM){
            return 0;
        }
        else if (jumlahHariSewa> MAKS_HARI_PINJAM){
            return ((jumlahHariSewa-MAKS_HARI_PINJAM)*DENDA_PER_HARI)*jmlhPinjam;
        }
        return 0;
    }
    public void tampil(){
        System.out.println("nama "+getObjPeminjam1().getNomorMahasiswa());
    }
}
