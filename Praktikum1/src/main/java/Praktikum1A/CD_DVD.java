
package Praktikum1A;

public class CD_DVD extends Perpustakaan{
    private String ISBN;
    private  String format;
    
    public CD_DVD(String ISBN,String format,String id_koleksi, String judul, String penerbit,String tahunTerbit){
        super(id_koleksi, judul, penerbit, tahunTerbit);
        this.ISBN=ISBN;
        this.format=format;
    }
    public void setISBN(String ISBN){
        this.ISBN=ISBN;
    }
    public String getISBN(){
        return ISBN;
    }
    public void setFormat(String format){
        this.format=format;
    }
    public String getFoString(){
        return format;
    }
}
