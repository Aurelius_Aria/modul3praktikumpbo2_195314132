
package Praktikum1A;

public class Buku extends Perpustakaan{
    private String ISBN;
    private String jumlahHalaman;
    
    public Buku(String ISBN, String jumlahHalaman,String id_koleksi, String judul, String penerbit,String tahunTerbit){
        super(id_koleksi, judul, penerbit, tahunTerbit);
        this.ISBN=ISBN;
        this.jumlahHalaman=jumlahHalaman;
    }
    public void setISBN(String ISBN){
        this.ISBN=ISBN;
    }
    public String getISBN(){
        return ISBN;
    }
    public void setJUmlahHalaman(String jumlahHalaman){
        this.jumlahHalaman=jumlahHalaman;
    }
    public String getJumlahHalaman(){
        return jumlahHalaman;
    }
}
