
package Praktikum1A;

public class Peminjam {
    protected String nama;
    protected String alamat;
    
    public Peminjam(){
        
    }
    
    public Peminjam(String nama, String alamat){
        this.nama=nama;
        this.alamat=alamat;
    }
    public void setNama(String nama){
        this.nama=nama;
    }
    public String getNama(){
        return nama;
    }
    public void setAlamat(String alamat){
        this.alamat=alamat;
    }
    public String getAlamat(){
        return alamat;
    }
    
}
