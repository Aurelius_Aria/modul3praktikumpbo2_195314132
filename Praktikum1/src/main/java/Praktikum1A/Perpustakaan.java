
package Praktikum1A;

public class Perpustakaan {
    protected String id_koleksi;
    protected String judul;
    protected String penerbit;
    protected String tahunTerbit;
    
    public Perpustakaan(){
        
    }
    public Perpustakaan(String id_koleksi, String judul, String penerbit,String tahunTerbit){
        this.id_koleksi=id_koleksi;
        this.judul=judul;
        this.penerbit=penerbit;
        this.tahunTerbit=tahunTerbit;
    }
    public void setIdKoleksi(String id_koleksi){
        this.id_koleksi=id_koleksi;
    }
    public String getIdKoleksi(){
        return id_koleksi;
    }
    public void setJudul(String judul){
        this.judul=judul;
    }
    public String getJudul(){
        return judul;
    }
    public void setPenerbit(String penerbit){
        this.penerbit=penerbit;
    }
    public String getPenerbit(){
        return penerbit;
    }
    public void setTahunTerbit(String tahunTerbit){
        this.tahunTerbit=tahunTerbit;
    }
    public String getTahunTerbit(){
        return tahunTerbit;
    }
}
