
public class Main {

    public static void main(String[] args) {

        UKM objUKM = new UKM();
        Penduduk[]anggota;
        anggota = new Penduduk [5];
        int n = 0;
        
        System.out.println("UKM Sanata Dharma ");
        System.out.println("------------------------------------------------");
        
        Mahasiswa ketua = new Mahasiswa ();
        ketua.setNama("Tony Stark");
        ketua.setNim("195314132");
        ketua.setTempatTanggalLahir("Jogja, 12 juni 2001");

        Mahasiswa sekre = new Mahasiswa ();
        sekre.setNama("Chriss");
        sekre.setNim("195314129");
        sekre.setTempatTanggalLahir("Purwekerto, 10 oktober 2001");
        
        objUKM.setKetua(ketua);
        objUKM.setSekretaris(sekre);
        objUKM.setNamaUnit("Fotografi");
        System.out.println("" +objUKM.getNamaUnit());
        System.out.println("-------------------------------------");
        System.out.println("Ketua");
        System.out.println("Ketua                   : " +objUKM.getKetua().getNama());
        System.out.println("NIM                     : " +ketua.getNim());
        System.out.println("Tempat Tanggal Lahir    : "+ketua.getTepatTanggalLahir());
        System.out.println("");
        System.out.println("Sekertaris");
        System.out.println("Sekretaris              : " +objUKM.getSekretaris().getNama());
        System.out.println("NIM                     : " +sekre.getNim());
        System.out.println("Tempat Tanggal Lahir    : "+sekre.getTepatTanggalLahir());
        System.out.println("-------------------------------------");
        System.out.println("");
        
        anggota[n] = new MasyarakatSekitar();
        ((MasyarakatSekitar)anggota[n]).setNama("Thor");
        ((MasyarakatSekitar)anggota[n]).setNomor(123);
        ((MasyarakatSekitar)anggota[n]).setTempatTanggalLahir("Jogja, 12 april 1999");
        n++;
        
        anggota[n] = new MasyarakatSekitar();
        ((MasyarakatSekitar)anggota[n]).setNama("Hulk");
        ((MasyarakatSekitar)anggota[n]).setNomor(165);
        ((MasyarakatSekitar)anggota[n]).setTempatTanggalLahir("Jakarta, 12 Desember 2000");
        n++;
        
        

    
        System.out.println("Anggota");
        for (int i = 0; i < n; i++) {
            if(anggota[i]instanceof Mahasiswa){
                System.out.println("Mahasiswa");
                System.out.println("Nama            : "+anggota[i].getNama());
                System.out.println("NIM             : "+((Mahasiswa)anggota[i]).getNim());
                System.out.println("Tanggal Lahir   : "+anggota[i].getTepatTanggalLahir());
                System.out.println("Iuran           : Rp"+((Mahasiswa)anggota[i]).hitungIuran());  
                System.out.println("");
            }
            else {
                System.out.println("Masyarakat Sekitar");
                 System.out.println("Nama           : " +anggota[i].getNama());
                System.out.println("NoKtp           : " +((MasyarakatSekitar)anggota[i]).getNomor());
                System.out.println("Tanggal Lahir   : " +anggota[i].getTepatTanggalLahir());
                System.out.println("Iuran           : Rp" +((MasyarakatSekitar)anggota[i]).hitungIuran());
                System.out.println("");
            
            }
        }
    }
}
